<?php

namespace MediaWiki\Extension\ResponsiveFrontend;

use Content;
use Html;
use MessageLocalizer;
use TextSlotDiffRenderer;

class ResponsiveTextSlotDiffRenderer extends TextSlotDiffRenderer {
	/** @var MessageLocalizer */
	protected $localizer;

	/**
	 * @param string $engine
	 * @param MessageLocalizer $localizer
	 */
	public function __construct( string $engine, MessageLocalizer $localizer ) {
		$this->setEngine( $engine );
		$this->localizer = $localizer;
	}

	/**
	 * @param Content|null $oldContent
	 * @param Content|null $newContent
	 * @return string
	 */
	public function getDiff( ?Content $oldContent = null, ?Content $newContent = null ) {
		$transformer = new DiffTransformer( $this->localizer );
		$oldResult = parent::getDiff( $oldContent, $newContent );
		$newResult = $transformer->transform( $oldResult, false );
		$newResult = Html::rawElement(
			"tr",
			[],
			Html::rawElement(
				"td",
				[ "colspan" => 4, "id" => "responsive-diffTd" ],
				$newResult
			)
		);
		return $newResult;
	}
}
