<?php

namespace MediaWiki\Extension\ResponsiveFrontend;

use Html;
use MessageLocalizer;
use Wikimedia\RemexHtml\Serializer\HtmlFormatter;
use Wikimedia\RemexHtml\Serializer\SerializerNode;

class DiffHeaderFormatter extends HtmlFormatter {

	/** @var MessageLocalizer */
	protected $localizer;

	/** @var string */
	private $imagepageLang = "";

	/** @var string */
	private $imagepageDir = "";

	/**
	 * @param MessageLocalizer $localizer
	 */
	public function __construct( MessageLocalizer $localizer ) {
		$this->localizer = $localizer;
	}

	/**
	 * @return string
	 */
	public function getImagepageDir() {
		return $this->imagepageDir;
	}

	/**
	 * @return string
	 */
	public function getImagepageLang() {
		return $this->imagepageLang;
	}

	/**
	 * @param SerializerNode $parent
	 * @param SerializerNode $node
	 * @param string $contents
	 *
	 * @return string
	 */
	public function element( SerializerNode $parent, SerializerNode $node, $contents ) {
		$nodeName = $node->name;
		if ( in_array( $nodeName, [ "html", "body", "head" ] ) ) {
			return $contents;
		}

		if ( isset( $node->attrs["class"] ) ) {
			$oldClass = $node->attrs["class"];
		} else {
			$oldClass = "";
		}

		if ( isset( $node->attrs["id"] ) ) {
			$oldId = $node->attrs["id"];
		} else {
			$oldId = "";
		}

		/* Workaround needed due to MediaWiki inserting an unclosed div on image pages
		 * See: https://gerrit.wikimedia.org/r/plugins/gitiles/mediawiki/core/+/refs/tags/1.36.3/includes/page/ImagePage.php#149
		 */
		if ( $oldId === "mw-imagepage-content" ) {
			$this->imagepageDir = $node->attrs["dir"];
			$this->imagepageLang = $node->attrs["lang"];
			return $contents;
		}

		if ( $nodeName === "a" && $oldId === "differences-prevlink" ) {
			$parent = clone $parent;
			$parent->attrs["data-prevlink"] = $node->attrs["href"];
			return parent::element( $parent, $node, $contents );
		}

		if ( ( strpos( $oldClass, "diff-otitle" ) !== false || $oldId === "mw-diff-otitle4" ) &&
			isset( $node->attrs["data-prevlink"] )
		) {
			$parent = clone $parent;
			$node = clone $node;
			$parent->attrs["data-prevlink"] = $node->attrs["data-prevlink"];
			unset( $node->attrs["data-prevlink"] );
			if ( strpos( $oldClass, "diff-otitle" ) !== false ) {
				$node->attrs["colspan"] = 2;
			}
			return parent::element( $parent, $node, $contents );
		}

		// otherwise, when there's no link
		if ( strpos( $oldClass, "diff-otitle" ) !== false ) {
			$node = clone $node;
			$node->attrs["colspan"] = 2;
			return parent::element( $parent, $node, $contents );
		}

		/* not that elegant, but we can avoid moving elements in the DOM tree */
		if ( $oldClass === "diff-title" ) {
			$node = clone $node;
			$ret = parent::element( $parent, $node, $contents );
			if ( isset( $node->attrs["data-prevlink"] ) ) {
				$newElement = Html::rawElement(
					"tr",
					[ "class" => "diff-prevlink-mobile" ],
					Html::rawElement(
						"td",
						[ "class" => "diff-prevlink-mobile-td", "colspan" => 4 ],
						Html::rawElement(
							"center",
							[],
							Html::rawElement(
								"a",
								[ "href" => $node->attrs["data-prevlink"] ],
								$this->localizer->msg( "previousdiff" )->parse()
							)
						)
					)
				);
				unset( $node->attrs["data-prevlink"] );
				$ret = $ret . $newElement;
			}

			return $ret . "<tr><td colspan='4'>";
		}

		if ( $oldClass === "patrollink-range" || $oldClass === "patrollink" ) {
			$parent = clone $parent;
			$parent->attrs["class"] = "diff-not-empty";
			return parent::element( $parent, $node, $contents );
		}

		if ( $oldId === "differences-nextlink" ) {
			$parent = clone $parent;
			$parent->attrs["class"] = "diff-not-empty";
			return parent::element( $parent, $node, $contents );
		}

		if ( $parent->attrs["class"] === "diff-title" ) {
			$node->attrs["colspan"] = "";
			return parent::element( $parent, $node, $contents );
		}

		if ( $oldId === "mw-diff-ntitle4" && $oldClass === "diff-not-empty" ) {
			$contents = str_replace( "&nbsp;", "", $contents );
			return parent::element( $parent, $node, $contents );
		}

		if ( $nodeName === "table" ) {
			$node->attrs["class"] = $oldClass . " diff-responsive-ready";
		}
		return parent::element( $parent, $node, $contents );
	}

	/**
	 * @param string|null $fragmentNamespace
	 * @param string|null $fragmentName
	 * @return string
	 */
	public function startDocument( $fragmentNamespace, $fragmentName ) {
		return "";
	}
}
