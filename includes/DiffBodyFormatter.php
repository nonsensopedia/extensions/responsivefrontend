<?php

namespace MediaWiki\Extension\ResponsiveFrontend;

use Html;
use MessageLocalizer;
use Wikimedia\RemexHtml\Serializer\HtmlFormatter;
use Wikimedia\RemexHtml\Serializer\SerializerNode;

class DiffBodyFormatter extends HtmlFormatter {
	/** @var MessageLocalizer */
	protected $localizer;

	/**
	 * @param MessageLocalizer $localizer
	 */
	public function __construct( MessageLocalizer $localizer ) {
		$this->localizer = $localizer;
	}

	/**
	 * @param SerializerNode $parent
	 * @param SerializerNode $node
	 * @param string $contents
	 *
	 * @return string
	 */
	public function element( SerializerNode $parent, SerializerNode $node, $contents ) {
		$nodeName = $node->name;

		if ( isset( $node->attrs["class"] ) ) {
			$oldClass = $node->attrs["class"];
		} else {
			$oldClass = "";
		}

		// just to make sure it won't remove something important
		if ( $nodeName === "table" && $oldClass === "table-removeme" ) {
			return $contents;
		}

		// WikiDiff2 compatibility
		if ( $oldClass === "mw-diff-movedpara-right" ) {
			$parent = clone $parent;
			$parent->attrs["data-marker"] = "⚫-right";
			$href = $node->attrs["href"];
			$msg = $this->localizer->msg( "responsiveFrontend-diff-movedpara-right" )->parse();
			$helpText = Html::rawElement(
				"a",
				[
					"class" => [ "diff-movedpara-lang-a", "diff-movedpara-lang-a-right" ],
					"href" => $href
				],
				Html::rawElement(
					"span",
					[ "class" => "diff-movedpara-lang-text" ],
					$msg
				)
			);
			return parent::element( $parent, $node, $contents ) . $helpText;
		}
		if ( $oldClass === "mw-diff-movedpara-left" ) {
			$parent = clone $parent;
			$parent->attrs["data-marker"] = "⚫-left";
			$href = $node->attrs["href"];
			$msg = $this->localizer->msg( "responsiveFrontend-diff-movedpara-left" )->parse();
			$helpText = Html::rawElement(
				"a",
				[
					"class" => [ "diff-movedpara-lang-a", "diff-movedpara-lang-a-left" ],
					"href" => $href
				],
				Html::rawElement(
					"span",
					[ "class" => "diff-movedpara-lang-text" ],
					$msg
				)
			);
			return parent::element( $parent, $node, $contents ) . $helpText;
		}

		if ( in_array( $nodeName, [ "head", "html", "body" ] ) ) {
			return $contents;
		}

		if ( !in_array( $nodeName, [ "td", "tr", "tbody" ] ) ) {
			return parent::element( $parent, $node, $contents );
		}

		$node = clone $node;
		$node->name = "div";
		$newClass = $oldClass . " ";
		switch ( $nodeName ) {
			case "td":
				$newClass .= "diff-former-td";
				// WikiDiff2 compatibility
				if ( strpos( $newClass, "diff-marker" ) !== false && !isset( $node->attrs["data-marker"] ) ) {
					if ( $contents === "+" || $contents === "−" ) {
						$node->attrs["data-marker"] = $contents;
						$contents = "";
					}
				}
				break;
			case "tr":
				$newClass .= "diff-line";
				break;
		}
		if ( str_replace( " ", "", $newClass ) !== false ) {
			$node->attrs["class"] = $newClass;
		}
		return parent::element( $parent, $node, $contents );
	}

	/**
	 * @param string|null $fragmentNamespace
	 * @param string|null $fragmentName
	 * @return string
	 */
	public function startDocument( $fragmentNamespace, $fragmentName ) {
		return "";
	}
}
