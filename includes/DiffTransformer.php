<?php

namespace MediaWiki\Extension\ResponsiveFrontend;

use Html;
use MessageLocalizer;
use Wikimedia\RemexHtml\Serializer\Serializer;
use Wikimedia\RemexHtml\Tokenizer\Tokenizer;
use Wikimedia\RemexHtml\TreeBuilder\Dispatcher;
use Wikimedia\RemexHtml\TreeBuilder\TreeBuilder;
use Xml;

/**
 * Tool for tranforming table-based diffs into div-based diffs
 * @newable
 */
class DiffTransformer {
	/** @var MessageLocalizer */
	protected $localizer;

	/**
	 * @param MessageLocalizer $localizer
	 */
	public function __construct( MessageLocalizer $localizer ) {
		$this->localizer = $localizer;
	}

	/**
	 * @param string $html
	 * @param bool $headersMode
	 *
	 * @return string
	 */
	public function transform( string $html, bool $headersMode ): string {
		if ( $headersMode ) {
			$formatter = new DiffHeaderFormatter( $this->localizer );
		} else {
			$html = Html::rawElement(
				"table",
				[ "class" => "table-removeme" ],
				Html::rawElement(
					"tbody",
					[],
					$html
				)
			);
			$formatter = new DiffBodyFormatter( $this->localizer );
		}
		$serializer = new Serializer( $formatter );
		$treeBuilder = new TreeBuilder(
			$serializer,
			[
				"ignoreErrors" => true,
				"ignoreNulls" => true
			]
		);
		$dispatcher = new Dispatcher( $treeBuilder );
		$tokenizer = new Tokenizer(
			$dispatcher,
			$html,
			[
				"ignoreErrors" => true,
				"skipPreprocess" => true,
				"ignoreNulls" => true
			]
		);
		$tokenizer->execute();
		$res = $serializer->getResult();
		if ( $headersMode && $formatter->getImagepageLang() !== "" ) {
			$res = Xml::openElement( 'div', [ 'id' => 'mw-imagepage-content',
				'lang' => $formatter->getImagepageLang(),
				'dir' => $formatter->getImagepageDir(),
				'class' => 'mw-content-' . $formatter->getImagepageDir() ] ) . $res;
		}
		return $res;
	}
}
