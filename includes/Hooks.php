<?php

namespace MediaWiki\Extension\ResponsiveFrontend;

use CodeContentHandler;
use ContentHandler;
use DifferenceEngine;
use EditPage;
use IContextSource;
use MediaWiki\Content\Hook\GetSlotDiffRendererHook;
use MediaWiki\Diff\Hook\ArticleContentOnDiffHook;
use MediaWiki\Hook\EditPage__showEditForm_initialHook;
use MediaWiki\Preferences\Hook\GetPreferencesHook;
use MediaWiki\Revision\SlotRecord;
use MediaWiki\Revision\RevisionRecord;
use MediaWiki\User\UserOptionsLookup;
use OutputPage;
use SlotDiffRenderer;
use User;
use WikitextContentHandler;

/**
 * Hook handlers for ResponsiveFrontend extension
 */
class Hooks implements
	GetSlotDiffRendererHook,
	ArticleContentOnDiffHook,
	EditPage__showEditForm_initialHook,
	GetPreferencesHook
	{

	public const RESPONSIVE_USE_FLEX_DIFFS = "responsive-use-flex-diffs";

	/** @var UserOptionsLookup */
	private $userOptLookup;

	/**
	 * @param UserOptionsLookup $userOptLookup
	 */
	public function __construct( UserOptionsLookup $userOptLookup ) {
		$this->userOptLookup = $userOptLookup;
	}

	/**
	 * ArticleContentOnDiff hook handler
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/ArticleContentOnDiff
	 *
	 * @param DifferenceEngine $differenceEngine
	 * @param OutputPage $out
	 * @return void
	 */
	public function onArticleContentOnDiff( $differenceEngine, $out ) {
		$revision = $differenceEngine->getNewRevision();
		if ( !$revision ) {
			return;
		}
		$content = $revision->getContent(
			SlotRecord::MAIN,
			RevisionRecord::FOR_THIS_USER,
			$differenceEngine->getAuthority()
		);
		if ( !$content ) {
			return;
		}
		$contentHandler = $content->getContentHandler();

		if ( !( $contentHandler instanceof WikitextContentHandler ) &&
			!( $contentHandler instanceof CodeContentHandler ) ) {
			return;
		}

		$useFlexDiffs = $this->userOptLookup->getBoolOption(
			$out->getUser(),
			self::RESPONSIVE_USE_FLEX_DIFFS
		);
		if ( !$useFlexDiffs ) {
			return;
		}
		$transformer = new DiffTransformer( $out );
		$contents = $out->getHTML();
		$newContents = $transformer->transform( $contents, true );
		$out->clearHTML();
		$out->addHtml( $newContents );
		$out->addModuleStyles( "ext.responsive.diffStyles" );
	}

	/**
	 * GetSlotDiffRenderer hook handler
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/GetSlotDiffRenderer
	 *
	 * @param ContentHandler $contentHandler
	 * @param SlotDiffRenderer &$slotDiffRenderer
	 * @param IContextSource $context
	 * @return bool|void
	 */
	public function onGetSlotDiffRenderer( $contentHandler, &$slotDiffRenderer, $context ) {
		if ( !( $contentHandler instanceof WikitextContentHandler ) &&
			!( $contentHandler instanceof CodeContentHandler ) ) {
			return;
		}
		$useFlexDiffs = $this->userOptLookup->getBoolOption(
			$context->getUser(),
			self::RESPONSIVE_USE_FLEX_DIFFS
		);

		if ( !$useFlexDiffs ) {
			return;
		}

		$context->getOutput()->addModuleStyles( "ext.responsive.diffStyles" );
		$engineType = $contentHandler->createDifferenceEngine( $context )->getEngine();
		$out = $context->getOutput();
		$slotDiffRenderer = new ResponsiveTextSlotDiffRenderer( $engineType, $out );
	}

	/**
	 * @param EditPage $editor
	 * @param OutputPage $out
	 */
	public function onEditPage__showEditForm_initial( $editor, $out ) {
		$out->addModuleStyles( "ext.responsive.diffStyles" );
	}

	/**
	 * Registers this extension's user preferences
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/GetPreferences
	 *
	 * @param User $user
	 * @param array &$preferences
	 */
	public function onGetPreferences( $user, &$preferences ) {
		$preferences[self::RESPONSIVE_USE_FLEX_DIFFS] = [
			"type" => "check",
			"label-message" => "responsiveFrontend-pref-use-flex-diffs",
			"help-message" => "responsiveFrontend-pref-use-flex-diffs-help",
			"section" => "rendering/diffs",
		];
	}
}
